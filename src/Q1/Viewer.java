package Q1;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Viewer extends JFrame{

		public Viewer(){
			createFrame();
		}
		
		public void createFrame(){
			JFrame frame = new JFrame("FrameDemo");
			frame.setSize(400, 400);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
			BorderLayout border = new BorderLayout();
			frame.setLayout(border);
															
			JPanel panel1 = new JPanel(new BorderLayout());
			panel1.setBorder(BorderFactory.createLineBorder(Color.BLUE));
			frame.add(panel1);
			
			JButton button1 = new JButton("RED");
			JButton button2 = new JButton("GREEN");
			JButton button3 = new JButton("BLUE");
			

			
			button1.addActionListener(new ActionListener(){
				 public void actionPerformed(ActionEvent e)
		            {
					 panel1.setBackground(Color.RED);
		            }
			});
			
			button2.addActionListener(new ActionListener(){
				 public void actionPerformed(ActionEvent e)
		            {
					 panel1.setBackground(Color.GREEN);
		            }
			});
			
			button3.addActionListener(new ActionListener(){
				 public void actionPerformed(ActionEvent e)
		            {
					 panel1.setBackground(Color.BLUE);
		            }
			});
			
			
			
			
			JPanel panel2 = new JPanel();
			panel2.setBorder(BorderFactory.createLineBorder(Color.GREEN));
			panel2.setLayout(new GridLayout(1,3));
			panel2.add(button1);
			panel2.add(button2);
			panel2.add(button3);

			frame.add(panel2,BorderLayout.SOUTH);


			
		}
		

		
		
		
}
