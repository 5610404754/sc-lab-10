package Q5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class Viewerfive extends JFrame{
	
	public Viewerfive(){
		createFrame();
	}
	
	public void createFrame(){
	JFrame frame = new JFrame("FrameDemo5");
	frame.setSize(400, 400);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	JPanel panel = new JPanel();
	
	JMenuBar menubar = new JMenuBar();
	

	JMenu menu = new JMenu("first menu");
	menubar.add(menu);
	
	JMenuItem item1 = new JMenuItem("RED");
	JMenuItem item2 = new JMenuItem("GREEN");
	JMenuItem item3 = new JMenuItem("BLUE");	

	menu.add(item1);
	menu.add(item2);
	menu.add(item3);
	
	
	
	frame.setJMenuBar(menubar);	
	frame.setVisible(true);
	frame.add(panel);
	item1.addActionListener(new ActionListener(){
			 public void actionPerformed(ActionEvent e)
	            {
				 panel.setBackground(Color.RED);
	            }
		});
	
	item2.addActionListener(new ActionListener(){
		 public void actionPerformed(ActionEvent e)
           {
			 panel.setBackground(Color.GREEN);
           }
	});
	
	item3.addActionListener(new ActionListener(){
		 public void actionPerformed(ActionEvent e)
           {
			 panel.setBackground(Color.BLUE);
           }
	});
	
	}

}
