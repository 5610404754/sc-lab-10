package Q2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Viewertwo {
	
	public Viewertwo(){
		createFrame();
	}
	
	public void createFrame(){
		JFrame frame = new JFrame("FrameDemo");
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		BorderLayout border = new BorderLayout();
		frame.setLayout(border);
		
		JPanel panel1 = new JPanel(new BorderLayout());
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		frame.add(panel1);
		
		JRadioButton button1 = new JRadioButton("RED");
		JRadioButton button2 = new JRadioButton("GREEN");
		JRadioButton button3 = new JRadioButton("BLUE");
		
		JPanel panel2 = new JPanel(new BorderLayout());
		panel2.setLayout(new GridLayout(1,3));
		panel2.setBorder(BorderFactory.createLineBorder(Color.GREEN));
		panel2.add(button1);
		panel2.add(button2);
		panel2.add(button3);		
		frame.add(panel2,BorderLayout.SOUTH);
		
	     ButtonGroup group = new ButtonGroup();
	     group.add(button1);
	     group.add(button2);
	     group.add(button3);
	     
	     button1.addActionListener(new ActionListener(){
			 public void actionPerformed(ActionEvent e)
	            {
				 panel1.setBackground(Color.RED);
	            }
		});
		
		button2.addActionListener(new ActionListener(){
			 public void actionPerformed(ActionEvent e)
	            {
				 panel1.setBackground(Color.GREEN);
	            }
		});
		
		button3.addActionListener(new ActionListener(){
			 public void actionPerformed(ActionEvent e)
	            {
				 panel1.setBackground(Color.BLUE);
	            }
		});

	
	
	
	
	
	
	}
	
	
	

	
	
	
	
	
}
