package Question7;

public class Model7 {
	private double balance;

	public Model7() {
		balance = 0;
	}

	public void deposit(double amount) {
		balance += amount;
	}
	
	public void withdraw(double amount){
		balance -= amount;

	}
	public double checkBalance(){
		return balance;
	}
}
