package Question7;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Question6.BankController6;

public class View7 {
	BankController6 control = new BankController6();

	public void createFrame() {
		
		
		JFrame frame = new JFrame("Frame Test7");
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLayout(null);
		
		
		JPanel panel1 = new JPanel(new BorderLayout());
		panel1.setBounds(0, 0, 600, 300);
		
		JTextArea textarea1 = new JTextArea("Outcome");
		panel1.add(textarea1,BorderLayout.WEST);
		
		JScrollPane scroll = new JScrollPane();
		
		JTextArea textarea2 = new JTextArea("History" +"\n");
		panel1.add(textarea2,BorderLayout.EAST);
		
		
		
		JTextField text1 = new JTextField("Insert");
		
		JButton button1 = new JButton("Deposit");
		JButton button2 = new JButton("Withdraw");
		JButton button3 = new JButton("CheckBalance");
		
		button1.addActionListener(new ActionListener(){			
			public void actionPerformed(ActionEvent e) {
				
				String income = text1.getText();
				double cash = Double.parseDouble(income);
				control.moneyIn(cash);
				control.checkMoney();
				textarea1.setText("Depositing " + control.toString(cash));
				textarea2.append("Deposit "+ control.toString(cash)+"\n");
			}
        });  
		
		button2.addActionListener(new ActionListener(){			
			public void actionPerformed(ActionEvent e) {
				
				String income = text1.getText();
				double cash = Double.parseDouble(income);
				if(cash >control.checkMoney()){
					textarea1.setText("Invalid ");
				}
				else{
				control.moneyOut(cash);
				control.checkMoney();
				textarea1.setText("Withdrawing " + control.toString(cash));
				textarea2.append("Widraw "+ control.toString(cash)+"\n");

				}
				
			}
        }); 
		
		button3.addActionListener(new ActionListener(){			
			public void actionPerformed(ActionEvent e) {
				double check = control.checkMoney();
				textarea1.setText("Your Balance is " + control.toString(check));
			}
        }); 

		
		JPanel panel2 = new JPanel(new GridLayout(1,4));
		panel2.setBounds(0,300, 600, 100);
		panel2.setBackground(Color.GREEN);
		
		panel2.add(text1);
		
		panel2.add(button1);
		panel2.add(button2);
		panel2.add(button3);

		
		frame.add(panel1);
		frame.add(panel2);
		
		

		
		
	
		
		

	}
}
