package Question7;

import Question6.BankModel6;
import Question6.View6;

public class Controller7 {

Model7 model = new Model7();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		View7 view = new View7();		
		view.createFrame();
	}
	
	public void moneyIn(double amount){
		model.deposit(amount);
	}
	
	public void moneyOut(double amount){
		model.withdraw(amount);
	}
	
	public double checkMoney(){
		return model.checkBalance();
	}
	
	public String toString(double cash){
		return " "+cash;
	}

}
