package Question6;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class View6 {
	BankController6 control = new BankController6();

	public void createFrame() {
		
		
		JFrame frame = new JFrame("Frame Test");
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLayout(null);
		
		
		JPanel panel1 = new JPanel(new BorderLayout());
		panel1.setBounds(0, 0, 600, 300);
		
		JLabel label1 = new JLabel("Outcome");
		panel1.add(label1,BorderLayout.WEST);
		
		JTextField text1 = new JTextField("Insert");
		
		JButton button1 = new JButton("Deposit");
		JButton button2 = new JButton("Withdraw");
		JButton button3 = new JButton("CheckBalance");
		
		button1.addActionListener(new ActionListener(){			
			public void actionPerformed(ActionEvent e) {
				
				String income = text1.getText();
				double cash = Double.parseDouble(income);
				control.moneyIn(cash);
				control.checkMoney();
				label1.setText("Depositing " + control.toString(cash));
			}
        });  
		
		button2.addActionListener(new ActionListener(){			
			public void actionPerformed(ActionEvent e) {
				
				String income = text1.getText();
				double cash = Double.parseDouble(income);
				if(cash >control.checkMoney()){
					label1.setText("Invalid ");
				}
				else{
				control.moneyOut(cash);
				control.checkMoney();
				label1.setText("Withdrawing " + control.toString(cash));
				}
				
			}
        }); 
		
		button3.addActionListener(new ActionListener(){			
			public void actionPerformed(ActionEvent e) {
				double check = control.checkMoney();
				label1.setText("Your Balance is " + control.toString(check));
			}
        }); 

		
		JPanel panel2 = new JPanel(new GridLayout(1,4));
		panel2.setBounds(0,300, 600, 100);
		panel2.setBackground(Color.GREEN);
		
		panel2.add(text1);
		
		panel2.add(button1);
		panel2.add(button2);
		panel2.add(button3);

		
		frame.add(panel1);
		frame.add(panel2);
		
		

		
		
	
		
		

	}
}
