package Question6;

public class BankModel6 {
	private double balance;

	public BankModel6() {
		balance = 0;
	}

	public void deposit(double amount) {
		balance += amount;
	}
	
	public void withdraw(double amount){
		balance -= amount;

	}
	public double checkBalance(){
		return balance;
	}
}
