package Question6;


public class BankController6 {
	
	BankModel6 model = new BankModel6();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		View6 view = new View6();		
		view.createFrame();
	}
	
	public void moneyIn(double amount){
		model.deposit(amount);
	}
	
	public void moneyOut(double amount){
		model.withdraw(amount);
	}
	
	public double checkMoney(){
		return model.checkBalance();
	}
	
	public String toString(double cash){
		return " "+cash;
	}
}
