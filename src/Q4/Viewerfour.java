package Q4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Viewerfour {
	

	public Viewerfour(){
		createFrame();
		
	}
	public void createFrame(){
		JFrame frame = new JFrame("FrameDemo");
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLayout(null);
		
		JPanel panel1 = new JPanel();
		panel1.setBackground(Color.BLUE);
		panel1.setBounds(0,0,400,250);
		frame.add(panel1);
		
		
		
		JPanel panel2 = new JPanel();
		panel2.setBackground(Color.MAGENTA);

		panel2.setBounds(0, 250, 400, 150);
		
		JComboBox combo = new JComboBox();
		combo.setBounds(0, 0, 30, 45);
		combo.addItem("RED");
		combo.addItem("GREEN");
		combo.addItem("BLUE");
		panel2.add(combo);
		frame.add(panel2);
		
		combo.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
	        {
			 if(combo.getSelectedItem()=="RED"){
				 panel1.setBackground(Color.RED);
			 }
			 if(combo.getSelectedItem()=="GREEN"){
				 panel1.setBackground(Color.GREEN);
			 }
			 if(combo.getSelectedItem()=="BLUE"){
				 panel1.setBackground(Color.BLUE);
			 }
	        }
	});
		
	}
}
