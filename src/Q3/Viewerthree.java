package Q3;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Viewerthree {
	JCheckBox box1 = new JCheckBox("RED");
	JCheckBox box2 = new JCheckBox("GREEN");
	JCheckBox box3 = new JCheckBox("BLUE");
	JPanel panel1 = new JPanel(new BorderLayout());
	ActionListener listener;

	public Viewerthree(){
		
		class butttonListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setBack();
			}
			
		}
		
		listener = new butttonListener();
		createFrame();
	}
	public void setBack(){
		if(box1.isSelected()){
			panel1.setBackground(Color.RED);
		}
		if(box2.isSelected()){
			panel1.setBackground(Color.GREEN);
		}
		if(box3.isSelected()){
			panel1.setBackground(Color.BLUE);
		}
		if(box1.isSelected()&&box2.isSelected()){
			panel1.setBackground(Color.YELLOW);
		}
		if(box1.isSelected()&&box3.isSelected()){
			panel1.setBackground(Color.MAGENTA);
		}
		if(box2.isSelected()&&box3.isSelected()){
			panel1.setBackground(Color.CYAN);
		}
		if(box1.isSelected()&&box2.isSelected()&&box3.isSelected()){
			panel1.setBackground(Color.WHITE);
		}
		if(!(box1.isSelected()||box2.isSelected()||box3.isSelected())){
			panel1.setBackground(null);
		}
		
	}	

	private void createFrame() {
		JFrame frame = new JFrame("FrameDemo");
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		BorderLayout border = new BorderLayout();
		frame.setLayout(border);
		
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		frame.add(panel1);
		
		
		
		JPanel panel2 = new JPanel(new BorderLayout());
		panel2.setLayout(new GridLayout(1,3));
		panel2.setBorder(BorderFactory.createLineBorder(Color.GREEN));
		panel2.add(box1);
		box1.addActionListener(listener);
		panel2.add(box2);
		box2.addActionListener(listener);
		panel2.add(box3);	
		box3.addActionListener(listener);
		frame.add(panel2,BorderLayout.SOUTH);
		
		
	
	}
	
	
}
